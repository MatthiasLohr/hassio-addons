# Contributing to Energy Manager for Home Assistant

## Configuration Options

The *Energy Manager for Home Assistant* provides the following possibilities to be configured, in order of their precedence:

  * [CLI arguments](#cli-arguments)
  * [environment variables](#environment-variables)
  * [configuration file](#configuration-file)


### CLI arguments

Run `energy-manager -h` to see all available CLI arguments.


### Environment Variables

| Variable Name     | Description                                                                                                | Default Value                       |
|-------------------|------------------------------------------------------------------------------------------------------------|-------------------------------------|
| `HA_API_BASE_URL` | URL to the root of the Home Assistant API endpoints.                                                       | `"http://homeassistant.local:8123"` |
| `HA_API_TOKEN`    | Access token to authorize against the Home Assistant API.                                                  | `None`                              |
| `LOG_LEVEL`       | Define the log level of this application. Choose from `DEBUG`, `INFO`, `WARNING`, `ERROR` and `CRITICAL` . | `"INFO"`                            |

### configuration-file
