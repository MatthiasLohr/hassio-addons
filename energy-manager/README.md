# Energy Manager for Home Assistant

This add-on allows to define entities to be automatically controlled depending on available energy surplus, e.g., produced by a photovoltaic system.


## Configuration

```yaml
## The source entity providing the current available energy surplus.
energy_surplus_entity_id: sensor.power_grid_export

## The minimum time required until enabling/disabling an entity will impact the available energy surplus.
## E.g., when energy surplus is only reported every 30 seconds to Home Assistant, this value should be >= 30 here. 
minimal_enabling_time: 0
minimal_disabling_time: 0

## Schedule definition for the devices (represented by their entities) to be managed.
## See table below for all available attributes per schedule entry.
schedule:
  - name: Test Switch
    entity_id: switch.energy_manager_test
    expected_energy_draw: 10
    energy_surplus_time_window: 300
```


### Schedule Entry Attributes

| Name                         | Required | Description                                                                                                                                                          | Default Value |
|------------------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|
| `name`                       | Yes      | Descriptive name of the schedule entry                                                                                                                               | N.A.          |
| `entity_id`                  | Yes      | The Home Assistant entity_id of the entity to be controlled                                                                                                          | N.A.          |
| `expected_energy_draw`       | Yes      | The amount of energy this device is expected to draw                                                                                                                 | N.A.          |
| `energy_surplus_time_window` | No       | Amount of seconds in which, in average, the energy surplus must excee `expected_energy_draw` for the device to be enabled.                                           | `0`           |
| `enable`                     | No       | Should this device be enabled by the Energy Manager for Home Assistant?                                                                                              | `True`        |
| `disable`                    | No       | Should this device be disabled by the Energy Manager for Home Assistant?                                                                                             | `True`        |
| `state_enabled`              | No       | The state which is interpreted as the entity being enabled. If not specified, it will use default values depending on the entity platform (e.g. `on` for switches).  | `None`        |
| `state_disabled`             | No       | The state which is interpreted as the entity being enabled. If not specified, it will use default values depending on the entity platform (e.g. `off` for switches). | `None`        |
| `expected_enabling_time`     | No       | Expected time (in seconds) until the device will draw the maximum power.                                                                                             | `0`           |
| `expected_disabling_time`    | No       | Expected time (in seconds) until the device will stop drawing power.                                                                                                 | `0`           |
| `minimal_enabled_time`       | No       | Once enabled, the device will kept enabled for at least the specified amount of time (in seconds)                                                                    | `0`           |
| `minimal_disabled_time`      | No       | Once disable, the device will kept disabled for at least the specified amount of time (in seconds)                                                                   | `0`           |
