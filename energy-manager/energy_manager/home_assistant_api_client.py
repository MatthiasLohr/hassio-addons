import json
import logging
from typing import Any, Dict

import requests

logger = logging.getLogger(__name__)


class HomeAssistantAPIClient(object):
    def __init__(self, api_base_url: str, api_token: str | None = None) -> None:
        self._api_base_url = api_base_url
        self._api_token = api_token

    def _build_endpoint_url(self, endpoint: str) -> str:
        return f"{self._api_base_url}/{endpoint}"

    def _build_default_headers(self) -> Dict[str, str]:
        headers = {"Content-Type": "application/json"}

        if self._api_token is not None:
            headers.update({"Authorization": f"Bearer {self._api_token}"})

        return headers

    def get_entity_state(self, entity_id: str) -> Any:
        response = requests.get(
            url=self._build_endpoint_url(f"states/{entity_id}"), headers=self._build_default_headers()
        )
        return response.json()

    def call_service(self, domain: str, service: str, data: Dict[str, Any] | None = None) -> Any:
        if data is None:
            data = {}

        response = requests.post(
            url=self._build_endpoint_url(f"services/{domain}/{service}"),
            headers=self._build_default_headers(),
            data=json.dumps(data),
        )
        return response.json()
