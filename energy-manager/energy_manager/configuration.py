import json
import logging
from os import getenv
from os.path import splitext
from typing import Any, Dict, List

import yaml

from .schedule_entry import ScheduleEntry

DEFAULT_HA_API_BASE_URL = "http://homeassistant.local:8123"

logger = logging.getLogger(__name__)


class Configuration(object):
    def __init__(self, config_dict: Dict[str, Any]) -> None:
        self._config_dict = config_dict

        self._schedule = [
            ScheduleEntry(
                name=entry["name"],
                entity_id=entry["entity_id"],
                expected_energy_draw=entry["expected_energy_draw"],
                energy_surplus_time_window=entry["energy_surplus_time_window"],
                enable=entry.get("enable", True),
                disable=entry.get("disable", True),
                state_enabled=entry.get("state_enabled"),
                state_disabled=entry.get("state_disabled"),
                expected_enabling_time=max(
                    entry.get("expected_enabling_time", 0), self._config_dict.get("minimal_enabling_time", 0)
                ),
                expected_disabling_time=max(
                    entry.get("expected_disabling_time", 0), self._config_dict.get("minimal_disabling_time", 0)
                ),
                minimal_enabled_time=int(entry.get("minimal_enabled_time", 0)),
                minimal_disabled_time=int(entry.get("minimal_disabled_time", 0)),
            )
            for entry in self._config_dict["schedule"]
        ]

    @classmethod
    def from_file(cls, filename: str) -> "Configuration":
        logger.debug(f"Loading configuration from file {filename}")
        _, ext = splitext(filename.lower())
        if ext == ".json":
            return cls.from_json_file(filename)
        elif ext in (".yaml", ".yml"):
            return cls.from_yaml_file(filename)
        else:
            raise ValueError(f"Unsupported extension {ext} for Energy Manager configuration")

    @staticmethod
    def from_json_file(filename: str) -> "Configuration":
        with open(filename) as fp:
            config_dict = json.load(fp)
        return Configuration(config_dict)

    @staticmethod
    def from_yaml_file(filename: str) -> "Configuration":
        with open(filename) as fp:
            config_dict = yaml.safe_load(fp)
        return Configuration(config_dict)

    @property
    def ha_api_base_url(self) -> str:
        return getenv("HA_API_BASE_URL", DEFAULT_HA_API_BASE_URL)

    @property
    def ha_api_token(self) -> str | None:
        return getenv("HA_API_TOKEN")

    @property
    def loop_interval(self) -> int:
        return int(getenv("LOOP_INTERVAL", 15))  # TODO generalize

    @property
    def energy_surplus_entity_id(self) -> str:
        return str(self._config_dict["energy_surplus_entity_id"])

    @property
    def schedule(self) -> List[ScheduleEntry]:
        return self._schedule
