import logging
from argparse import ArgumentParser
from signal import SIGINT, signal

from .configuration import Configuration
from .manager import Manager

logger = logging.getLogger(__name__)


def main() -> int:
    argument_parser = ArgumentParser()
    argument_parser.add_argument("-c", "--configuration", default="energy-manager.yaml")
    args = argument_parser.parse_args()

    logger.info("starting up Energy Manager for Home Assistant")

    config = Configuration.from_file(args.configuration)

    manager = Manager(config)
    signal(SIGINT, lambda sig, frame: manager.stop())

    manager.run()

    return 0
