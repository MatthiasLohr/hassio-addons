import logging
from collections import deque
from datetime import datetime, timedelta
from math import ceil
from threading import Event
from time import time
from typing import Any, Dict

from .configuration import Configuration
from .errors import InsufficientDataError
from .home_assistant_api_client import HomeAssistantAPIClient
from .schedule_entry import ScheduleEntry

logger = logging.getLogger(__name__)


class Manager(object):
    def __init__(self, configuration: Configuration):
        self._config = configuration

        self._ha_client = HomeAssistantAPIClient(
            api_base_url=self._config.ha_api_base_url, api_token=self._config.ha_api_token
        )
        self._energy_surplus_cache: deque[float] = deque(maxlen=round(43200 / self._config.loop_interval))
        self._stop_event = Event()
        self._enable_next_after = time()
        self._disable_next_after = time()

    def run(self) -> None:
        self._stop_event.clear()

        while not self._stop_event.is_set():
            _last_loop_start_time = time()

            current_energy_surplus = self.get_current_energy_surplus()
            logger.debug(f"Current energy surplus: {current_energy_surplus}")
            self._energy_surplus_cache.appendleft(current_energy_surplus)

            self._manage_schedule()

            self._stop_event.wait(max(time() - _last_loop_start_time + self._config.loop_interval, 0))

    def _manage_schedule(self) -> None:
        for schedule_entry in self._config.schedule:
            logger.debug(f"Checking schedule entry '{schedule_entry}'")
            entity_state = self._ha_client.get_entity_state(schedule_entry.entity_id)
            self._manage_schedule_entry(schedule_entry, entity_state)

    def _manage_schedule_entry(self, schedule_entry: ScheduleEntry, entity_state: Dict[str, Any]) -> None:
        entity_state_last_changed = datetime.fromisoformat(entity_state["last_changed"])

        try:
            state_disabled = schedule_entry.state_disabled
            state_enabled = schedule_entry.state_enabled
        except RuntimeError as e:
            logger.error(f"Cannot determin device enabled/disabled state: {str(e)}")
            return

        if entity_state["state"] == state_disabled:
            logger.debug(f"{schedule_entry} is disabled")

            # should we enable this entry at all?
            if not schedule_entry.enable:
                return

            # has it been disabled shortly?
            self._enable_next_after = max(
                self._enable_next_after,
                (entity_state_last_changed + timedelta(seconds=schedule_entry.expected_enabling_time)).timestamp(),
            )

            # can anything be enabled?
            if self._enable_next_after > time():
                logger.debug(f"nothing should be enabled until {datetime.fromtimestamp(self._enable_next_after)}")
                return

            # can this be enabled?
            if (
                entity_state_last_changed + timedelta(seconds=schedule_entry.minimal_disabled_time)
            ).timestamp() > time():
                logger.debug(
                    f"{schedule_entry} should not be enabled"
                    f" as it has been disabled {round(time()-entity_state_last_changed.timestamp())} seconds before"
                    f" which is less than {schedule_entry.minimal_disabled_time} seconds as required"
                )
                return

            # should this be enabled?
            try:
                surplus_average = self._get_surplus_average(schedule_entry.energy_surplus_time_window)
            except InsufficientDataError:
                logger.warning(f"Insufficient data to build average for enabling {schedule_entry}, skipping")
                return

            if surplus_average > schedule_entry.expected_energy_draw:
                logger.info(
                    f"Energy surplus average of {schedule_entry.energy_surplus_time_window} seconds"
                    f" is {surplus_average}, enabling {schedule_entry}"
                )
                try:
                    self.schedule_entry_enable(schedule_entry)
                except RuntimeError as e:
                    logger.critical(f"Could not enable {schedule_entry}: {e}")

        elif entity_state["state"] == state_enabled:
            logger.debug(f"{schedule_entry} is enabled")

            # should we disable this entry at all?
            if not schedule_entry.disable:
                return

            # has it been enabled shortly?
            self._disable_next_after = max(
                self._disable_next_after,
                (entity_state_last_changed + timedelta(seconds=schedule_entry.expected_enabling_time)).timestamp(),
            )

            # can anything be disabled?
            if self._disable_next_after > time():
                logger.debug(f"nothing should be disabled until {datetime.fromtimestamp(self._disable_next_after)}")
                return

            # can this be disabled?
            if (
                entity_state_last_changed + timedelta(seconds=schedule_entry.minimal_enabled_time)
            ).timestamp() > time():
                logger.debug(
                    f"{schedule_entry} should not be disabled"
                    f" as it has been enabled {round(time()-entity_state_last_changed.timestamp())} seconds before"
                    f" which is less than {schedule_entry.minimal_enabled_time} seconds as required"
                )
                return

            # should this be disabled?
            try:
                surplus_average = self._get_surplus_average(schedule_entry.energy_surplus_time_window)
            except InsufficientDataError:
                logger.warning(f"Insufficient data to build average for disabling {schedule_entry}, skipping")
                return

            if surplus_average <= 0:
                logger.info(
                    f"Energy surplus average of {schedule_entry.energy_surplus_time_window} seconds"
                    f" is {surplus_average}, disabling {schedule_entry}"
                )
                try:
                    self.schedule_entry_disable(schedule_entry)
                except RuntimeError as e:
                    logger.critical(f"Could not disable {schedule_entry}: {e}")

        else:
            logger.warning(
                f"current state '{entity_state["state"]}' of {schedule_entry}"
                f" does not match state_enabled/state_disabled, ignoring"
            )

    def _get_surplus_average(self, time_window: int) -> float:
        energy_surplus_cache_elements_required = ceil(time_window / self._config.loop_interval)
        if len(self._energy_surplus_cache) < energy_surplus_cache_elements_required:
            raise InsufficientDataError()

        return (
            sum([self._energy_surplus_cache[x] for x in range(energy_surplus_cache_elements_required)])
            / energy_surplus_cache_elements_required
        )

    def schedule_entry_enable(self, schedule_entry: ScheduleEntry) -> bool:
        if schedule_entry.entity_domain == "switch":
            self._ha_client.call_service("switch", "turn_on", {"entity_id": schedule_entry.entity_id})
            return True  # TODO actually check for success
        else:
            raise RuntimeError(f"Missing support to enable {schedule_entry} ({schedule_entry.entity_domain} domain)")

    def schedule_entry_disable(self, schedule_entry: ScheduleEntry) -> bool:
        if schedule_entry.entity_domain == "switch":
            self._ha_client.call_service("switch", "turn_off", {"entity_id": schedule_entry.entity_id})
            return True  # TODO actually check for success
        else:
            raise RuntimeError(f"Missing support to enable {schedule_entry} ({schedule_entry.entity_domain} domain)")

    def stop(self) -> None:
        self._stop_event.set()

    def get_current_energy_surplus(self) -> float:
        return float(self._ha_client.get_entity_state(self._config.energy_surplus_entity_id)["state"])
