import logging
from os import getenv

# configure logging formatter
logging_formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# configure stream handler
logging_stream_handler = logging.StreamHandler()
logging_stream_handler.setFormatter(logging_formatter)
# configure root logger
root_logger = logging.getLogger(__name__)
root_logger.addHandler(logging_stream_handler)
root_logger.setLevel(getenv("LOG_LEVEL", "INFO"))
