class ScheduleEntry(object):
    def __init__(
        self,
        name: str,
        entity_id: str,
        expected_energy_draw: float,
        energy_surplus_time_window: int,
        enable: bool = True,
        disable: bool = True,
        state_enabled: str | None = None,
        state_disabled: str | None = None,
        expected_enabling_time: int = 0,
        expected_disabling_time: int = 0,
        minimal_enabled_time: int = 0,
        minimal_disabled_time: int = 0,
    ) -> None:
        self._name = name
        self._entity_id = entity_id
        self._expected_energy_draw = expected_energy_draw
        self._enable = enable
        self._disable = disable
        self._energy_surplus_time_window = energy_surplus_time_window
        self._state_enabled = state_enabled
        self._state_disabled = state_disabled
        self._expected_enabling_time = expected_enabling_time
        self._expected_disabling_time = expected_disabling_time
        self._minimal_enabled_time = minimal_enabled_time
        self._minimal_disabled_time = minimal_disabled_time

    @property
    def name(self) -> str:
        return self._name

    @property
    def entity_id(self) -> str:
        return self._entity_id

    @property
    def expected_energy_draw(self) -> float:
        return self._expected_energy_draw

    @property
    def enable(self) -> bool:
        return self._enable

    @property
    def disable(self) -> bool:
        return self._disable

    @property
    def energy_surplus_time_window(self) -> int:
        return self._energy_surplus_time_window

    @property
    def state_enabled(self) -> str:
        if self._state_enabled is not None:
            return self._state_enabled
        elif self.entity_domain == "switch":
            return "on"
        else:
            raise RuntimeError(
                f"neither specific state_enabled setting for {str(self)}"
                f" nor default setting for platform {self.entity_domain}"
            )

    @property
    def state_disabled(self) -> str:
        if self._state_disabled is not None:
            return self._state_disabled
        elif self.entity_domain == "switch":
            return "off"
        elif self.entity_domain == "climate":
            return "off"
        else:
            raise RuntimeError(
                f"neither specific state_disabled setting for {str(self)}"
                f" nor default setting for platform {self.entity_domain}"
            )

    @property
    def expected_enabling_time(self) -> int:
        return self._expected_enabling_time

    @property
    def expected_disabling_time(self) -> int:
        return self._expected_disabling_time

    @property
    def minimal_enabled_time(self) -> int:
        return self._minimal_enabled_time

    @property
    def minimal_disabled_time(self) -> int:
        return self._minimal_disabled_time

    @property
    def entity_domain(self) -> str:
        return self._entity_id.split(".", 2)[0]

    def __str__(self) -> str:
        return f"{self._name} ({self._entity_id})"
