# Matthias Lohr's Home Assistant OS Add-ons

This repository contains my custom Home Assistant OS Add-ons which I'm happy to share wih you.
If you want to use one of my add-on, please add my repository to your Home Assistant instance.

The repository URL is `https://gitlab.com/MatthiasLohr/hassio-addons.git`.
You can also use the following link to add the repository:

[![Open your Home Assistant instance and show the add add-on repository dialog with a specific repository URL pre-filled.](https://my.home-assistant.io/badges/supervisor_add_addon_repository.svg)](https://my.home-assistant.io/redirect/supervisor_add_addon_repository/?repository_url=https%3A%2F%2Fgitlab.com%2FMatthiasLohr%2Fhassio-addons.git)

## Add-ons

### Energy Manager

The [Energy Manager](./energy_manager/) allows to define entities to be automatically controlled depending on available energy surplus, e.g., produced by a photovoltaic system.
